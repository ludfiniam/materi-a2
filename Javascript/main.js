// Single element

// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('h1'));

// Multi elemental 
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByClassName('item'));
// const items = document.querySelectorAll('.item');

// items.forEach((item) => console.log(item));


// const ul = document.querySelector('.item');
// ul.remove();
// ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

// const btn = document.querySelector('.btn');
// btn.style.background = 'red';
// btn.addEventListener('click', (e) => {
// 	e.preventDefault();
// 	document.querySelector('#my-form').style.background = '#ccc';
// 	document.querySelector('body').classList.add('bg-dark');
// 	document.querySelector('.item').lastElementChild.innerHTML='<h1>Hello</h1>';
// });

const myform = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const usersList = document.querySelector('#users');

myform.addEventListener('submit', onSubmit);

function onSubmit(e){
	e.preventDefault();
	if(nameInput.value ===''|| emailInput.value ===''){
		// alert('please enter fields');
		msg.innerHTML = 'Please enter all dields';
	}
	else{
		const li = document.createElement('li');
		li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`));
		
		usersList.appendChild(li);

		//clear field
		nameInput.value=''; 
		emailInput.value='';
		console.log('succes');
		
	}
}