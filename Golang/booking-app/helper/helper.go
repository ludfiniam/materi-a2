package helper

import "strings"

func ValidateUserInput(firstname string, lastname string, email string, userTicketcall uint, remainingTickets uint) (bool, bool, bool) {
	isValidName := len(firstname) >= 2 && len(lastname) >= 2
	isValidEmail := strings.Contains(email,"@")
	isValidTicket := userTicketcall > 0 && userTicketcall <= remainingTickets
	return isValidName, isValidEmail, isValidTicket
}