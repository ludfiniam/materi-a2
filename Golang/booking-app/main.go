package main

import (
"fmt"
"booking-app/helper"
"time"
"sync"
)

var conferenceName = "Go Conference"
const conferenceTickets int = 50
var RemainingTickets uint = 50
var bookings = make([]UserData, 0)

type UserData struct{
	firstName string
	lastName string
	Email string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main(){

	greetUser()

	// for{
		
		//call input

		firstName, lastName, Email, userTickets :=getInputData() 

		isValidName, isValidEmail, isValidTicket := helper.ValidateUserInput(firstName, lastName, Email, userTickets, RemainingTickets)
		
		if isValidName && isValidEmail && isValidTicket{
			//Logic ticket
			bookTTicket(userTickets, firstName, lastName, Email)
			
			wg.Add(1)
			go sendTicket(userTickets, firstName, lastName, Email)

			//Function print firstname
			fmt.Printf("The first names of bookings are: %v\n", printFirstName())

			if RemainingTickets == 0 {
			//End program
				fmt.Println("Our conference is booked out. Come back next time.")
				// break
			}
		} else if userTickets == RemainingTickets{
 			//do something else
		} else{
			if !isValidName {
				fmt.Println("first name or last name you entered to short")
			}
			if !isValidEmail {
				fmt.Println("email doesn'tt contain @ sign")
			}
			if !isValidTicket {
				fmt.Println("number tickets you entered is INVALID only number please")
			}
		}
	// }
		wg.Wait()
}


func greetUser() {
	fmt.Printf("Wellcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, RemainingTickets)
	fmt.Println("Get your ticket here to attend")
}

func printFirstName() []string {
	firstNames :=[]string{}
	for _, booking := range bookings{
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames		
}


func getInputData() (string, string, string, uint){
	var firstName string
	var lastName string
	var Email string
	var userTickets uint
		//ask user for yheir name
	fmt.Println("Enter your first name please :")
	fmt.Scan(&firstName)

	fmt.Println("Enter your last name please :")
	fmt.Scan(&lastName)

	fmt.Println("Enter your Email address please :")
	fmt.Scan(&Email)

	fmt.Println("Enter your booking tickets please :")
	fmt.Scan(&userTickets)

	return firstName, lastName, Email, userTickets
	
}

func bookTTicket(userTickets uint, firstName string, lastName string, Email string) {
	RemainingTickets = RemainingTickets - userTickets
	//create map for user
	var userData= UserData {
		firstName: firstName,
		lastName: lastName,
		Email: Email,
		numberOfTickets: userTickets,
	}
	// userData["firstName"] = firstName
	// userData["lastName"] = lastName
	// userData["email"] = Email
	// userData["numberOfTickets"] = strconv.FormatUint(uint64(userTickets), 10)

	bookings = append(bookings, userData)

	fmt.Printf("List of bookings %v\n", bookings)

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, Email)
	fmt.Printf("%v tickets remaining for %v\n", RemainingTickets, conferenceName)

}

func sendTicket(userTickets uint, firstName string, lastName string, Email string){
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	fmt.Println("##################")
	fmt.Printf("Sending ticket: %v\n to email address %v\n", ticket, Email)
	fmt.Println("##################")
	wg.Done()
}